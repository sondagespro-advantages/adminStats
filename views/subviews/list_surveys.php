<h1><?php echo gT('Surveys') ?></h1>

<?php
    //echo CHtml::tag('pre',array(),print_r($aSurveys,1));
    $dataProvider=new CArrayDataProvider($aSurveys, array(
        'keyField' => 'sid',
        'caseSensitiveSort'=>false,
        'sort'=>array(
            'attributes'=>array(
                 'sid', 'surveyls_title','responsesCount','tokensCount','participationRate'
            ),
        ),
        'pagination'=>array(
            'pageSize'=>20,
        ),
    ));
    $this->widget('bootstrap.widgets.TbGridView', array(
        'dataProvider'=>$dataProvider,
        'ajaxUpdate' => true,
        'rowCssClassExpression'=>'$data["responsesCount"]==0?"hidden hide":""',
        'columns'=>array(
            array(
                'name'=>'sid',
                'sortable'=>true,
                'header'=>gT("ID"),
                'type' => 'raw',
                'value'=>'CHtml::link($data["sid"],array("plugins/direct","plugin"=>"adminStats","function"=>"participation","sid"=>$data["sid"]))',
                'footer'=>"<strong>".$translate->gT("Total")."</strong>",
            ),
            array(
                'name'=>'surveyls_title',
                'sortable'=>true,
                'header'=>gT("Title"),
                'value'=>'$data["title"]',
            ),
            array(
                'name'=>'tokensCount',
                'sortable'=>true,
                'header'=>$translate->gT("Mails sent"),
                'value'=>'($data["tokensCount"] ? $data["tokensCount"] : "/");',
                'footer'=>$footer['tokensCount'],
            ),
            array(
                'name'=>'responsesCount',
                'sortable'=>true,
                'header'=>$translate->gT("Responses"),
                'value'=>'$data["responsesCount"]',
                'footer'=>$footer['responsesCount'],
            ),
            array(
                'name'=>'participationRate',
                'sortable'=>true,
                'header'=>$translate->gT("Participation rate"),
                'value'=>'is_numeric($data["participationRate"]) ? round($data["participationRate"]*100,0)."%" : "/";',
                'footer'=>is_numeric($footer['participationRate']) ? round($footer['participationRate']*100,0)."%" : $footer['participationRate'] ,
            ),
        ),
    ));
?>
